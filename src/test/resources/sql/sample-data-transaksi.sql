insert into nasabah (id, kode, nama, jenis_kelamin, tanggal_lahir, alamat, waktu_dibuat, terakhir_update) values 
('n001', '123450001', 'Nasabah 001', 'PRIA', '1990-01-01', 'Jakarta', '2020-10-10 05:05:05', '2020-10-10 15:15:15'),
('n002', '123450002', 'Nasabah 002', 'WANITA', '1991-02-01', 'Bandung', '2020-10-10 05:05:05', '2020-10-10 15:15:15'),
('n003', '123450003', 'Nasabah 003', 'PRIA', '1992-03-01', 'Semarang', '2020-10-10 05:05:05', '2020-10-10 15:15:15'),
('n004', '123450004', 'Nasabah 004', 'WANITA', '1993-04-01', 'Surabaya', '2020-10-10 05:05:05', '2020-10-10 15:15:15');

insert into rekening (id, id_nasabah, nomor, saldo) values 
('r101', 'n001', '9876000101', 100000),
('r102', 'n001', '9876000102', 120000),
('r201', 'n002', '9876000201', 200001),
('r301', 'n003', '9876000301', 300003);

insert into virtual_account(id, id_rekening, nomor, jenis_virtual_account, nilai_total, nilai_sisa, akumulasi_pembayaran, virtual_account_status) values 
('v101', 'r101', '6660001010001', 'CLOSED', 100000, 100000, 0, 'PAID_FULLY'),
('v102', 'r101', '6660001010002', 'OPEN', 100000, 85000, 15000, 'UNPAID'),
('v103', 'r102', '6660001020001', 'CLOSED', '200000', '200000', 0, 'UNPAID'),
('v104', 'r102', '6660001020002', 'INSTALLMENT', '1000000', '100000', 900000, 'UNPAID'),
('v201', 'r201', '6660002010001', 'CLOSED', '100000', '100000', 0, 'UNPAID'),
('v301', 'r201', '6660002010002', 'CLOSED', '100000', '100000', 0, 'UNPAID');