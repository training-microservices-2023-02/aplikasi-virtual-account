insert into permission (id, permission_label, permission_value) values 
('p001', 'View Transaksi', 'VIEW_TRANSAKSI'),
('p002', 'Edit Transaksi', 'EDIT_TRANSAKSI'),
('p003', 'View Nasabah', 'VIEW_NASABAH'),
('p004', 'Edit Transaksi', 'EDIT_NASABAH');

insert into role(id, name) values 
('r001', 'Staff'),
('r002', 'Supervisor'),
('r003', 'Manager');

insert into role_permission(id_role, id_permission) values 
('r001', 'p001'),
('r001', 'p003'),
('r002', 'p001'),
('r002', 'p003'),
('r002', 'p004');

insert into user(id, id_role, username, active) values 
('u001', 'r001', 'staff001', true),
('u002', 'r002', 'supervisor001', true),
('u003', 'r003', 'manager001', true);

insert into user_password(id, hashed_password) values 
('u001', '{noop}test123'),
('u002', '{noop}test123'),
('u003', '{noop}test123');
