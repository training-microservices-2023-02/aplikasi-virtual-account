package com.muhardin.endy.training.virtualaccount.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RunningNumberServiceTests {
    @Autowired private RunningNumberService runningNumberService;

    @Test
    public void testGenerate(){
        String num = runningNumberService.generate("COBA");
        System.out.println(num);
    }

    @Test
    public void testGenerateMultiuser() throws InterruptedException {
        int jumlahConcurrent = 5;
        int jumlahNumber = 50;

        for(int i=0; i<jumlahConcurrent; i++){
            new RunningNumberGenerator("TESTMULTI", runningNumberService, jumlahNumber)
            .start();
        }

        Thread.sleep(30 * 1000);
    }

    class RunningNumberGenerator extends Thread {
        
        private Integer repeat;
        private String prefix;
        private RunningNumberService service;

        public RunningNumberGenerator(String p, RunningNumberService service, Integer repeat) {
            this.prefix = p;
            this.service = service;
            this.repeat = repeat;
        }

        public void run(){
            for(int i = 0; i<repeat; i++){
                String rn = service.generate(prefix);
                System.out.println("Running Number : "+rn);
            }
        }
    }
}
