package com.muhardin.endy.training.virtualaccount.dao;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import com.muhardin.endy.training.virtualaccount.entity.Nasabah;
import com.muhardin.endy.training.virtualaccount.entity.Rekening;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/delete-data-transaksi.sql", 
        "classpath:/sql/sample-data-transaksi.sql"
    }
)
public class RekeningDaoTests {
    @Autowired private RekeningDao rekeningDao;

    @Test @Transactional
    public void testAuditLog(){
        Rekening r = new Rekening();
        r.setNomor("99900001");
        r.setSaldo(new BigDecimal("10000000"));

        Nasabah n = new Nasabah();
        n.setId("n001");
        r.setNasabah(n);

        rekeningDao.saveAndFlush(r);

        r.setSaldo(new BigDecimal("11000000"));
        rekeningDao.saveAndFlush(r);

        r.setNomor("99900002");
        r.setSaldo(new BigDecimal("12000000"));
        rekeningDao.saveAndFlush(r);

        rekeningDao.delete(r);

        Revisions<Long, Rekening> revisions = rekeningDao.findRevisions(r.getId());
        for(Revision<Long, Rekening> rev : revisions) {
            System.out.println("Rev # : "+rev.getRevisionNumber());
            System.out.println("Rev Type : "+rev.getMetadata().getRevisionType());
            System.out.println("Nomor : "+rev.getEntity().getNomor());
            System.out.println("Saldo : "+rev.getEntity().getSaldo());
        }
    }
}
