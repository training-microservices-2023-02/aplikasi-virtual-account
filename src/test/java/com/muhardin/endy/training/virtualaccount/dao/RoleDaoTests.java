package com.muhardin.endy.training.virtualaccount.dao;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import com.muhardin.endy.training.virtualaccount.entity.Permission;
import com.muhardin.endy.training.virtualaccount.entity.Role;
import com.muhardin.endy.training.virtualaccount.entity.User;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/delete-data-user-role-permission.sql", 
        "classpath:/sql/sample-data-user-role-permission.sql"
    }
)
public class RoleDaoTests {

    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;
    @Autowired private PermissionDao permissionDao;

    @Test @Transactional @Rollback(false)
    public void testAddPermissionToRole(){
        Role manager = roleDao.findById("r003").get();
        Permission viewNasabah = permissionDao.findById("p003").get();
        Permission editNasabah = permissionDao.findById("p004").get();

        // menambahkan permission ke role
        manager.getDaftarPermission().add(viewNasabah);
        manager.getDaftarPermission().add(editNasabah);
        roleDao.save(manager);
    }

    @Test @Transactional
    public void testGetRolePermission(){
        Role staff = roleDao.findById("r001").get();
        for(Permission p : staff.getDaftarPermission()) {
            System.out.println("Permission : "+p.getPermissionLabel());
        }

        Permission px = permissionDao.findById("p001").get();
        for(Role r : px.getRoles()){
            System.out.println("Role : "+r.getName());
        }
    }

    @Test @Transactional @Rollback(false)
    public void testDeleteRole(){
        User u001 = userDao.findById("u001").get();
        u001.setRole(null); // hapus dulu relasi yang mengarah ke role yang mau dihapus
        userDao.save(u001);
        
        Role staff = roleDao.findById("r001").get();
        roleDao.delete(staff);
    }

    @Test @Transactional @Rollback(false)
    public void testDeletePermissionFromRole(){
        Role supervisor = roleDao.findById("r002").get();
        Iterator<Permission> i = supervisor.getDaftarPermission().iterator();
        while(i.hasNext()) {
            Permission p = i.next();
            if("VIEW_TRANSAKSI".equals(p.getPermissionValue())) {
                System.out.println("Permission : "+p.getId());
                i.remove(); // role supervisor tidak boleh lagi mengakses view transaksi
            }
        }
        roleDao.save(supervisor);
    }

    @Test @Transactional @Rollback(false)
    public void testDeletePermission(){
        Role r2 = roleDao.findById("r002").get();
        Iterator<Permission> i = r2.getDaftarPermission().iterator();
        while(i.hasNext()) {
            Permission p = i.next();
            if("p004".equals(p.getId())) {
                System.out.println("Permission : "+p.getId());
                i.remove(); // hapus dulu relasi yang mengarah ke p004
            }
        }
        roleDao.save(r2);
        permissionDao.deleteById("p004");
    }

    @Test
    public void testCariRoleTanpaPermission(){
        List<Role> hasil = roleDao.findByDaftarPermissionIsEmpty();
        System.out.println(hasil.size());
        System.out.println("Role : "+hasil.get(0).getName());
        Assertions.assertFalse(hasil.isEmpty());
    }
}
