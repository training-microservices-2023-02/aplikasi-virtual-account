package com.muhardin.endy.training.virtualaccount.service;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;

import com.muhardin.endy.training.virtualaccount.dao.VirtualAccountDao;
import com.muhardin.endy.training.virtualaccount.entity.PembayaranTunai;
import com.muhardin.endy.training.virtualaccount.entity.VirtualAccount;
import com.muhardin.endy.training.virtualaccount.exception.PembayaranBerlebihException;
import com.muhardin.endy.training.virtualaccount.exception.SudahLunasException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/delete-data-transaksi.sql", 
        "classpath:/sql/sample-data-transaksi.sql"
    }
)
public class PembayaranServiceTests {
    @Autowired private PembayaranService pembayaranService;
    @Autowired private VirtualAccountDao virtualAccountDao;

    @Test @Rollback(false)
    public void testPembayaranTunai() throws SudahLunasException {
        VirtualAccount va = virtualAccountDao.findById("v102").get();

        PembayaranTunai p = new PembayaranTunai();
        p.setVirtualAccount(va);
        p.setNilai(new BigDecimal(10000));
        p.setDibayar(new BigDecimal(10000)); 

        pembayaranService.bayar(p);
        
        /*
        try {
            pembayaranService.bayar(p);
        } catch (SudahLunasException e) {
            // biasanya ini menampilkan pesan error ke user
            // tidak perlu ditulis ke log file, karena ini proses bisnis biasa
            log.info("VA sudah lunas");
        } catch (PembayaranBerlebihException e) {
            // biasanya ini menampilkan pesan error ke user
            // tidak perlu ditulis ke log file, karena ini proses bisnis biasa
            log.info("bayar kebanyakan");
        }
         */
    }
}
