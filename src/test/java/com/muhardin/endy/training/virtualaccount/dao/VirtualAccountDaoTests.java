package com.muhardin.endy.training.virtualaccount.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import com.muhardin.endy.training.virtualaccount.entity.JenisVirtualAccount;
import com.muhardin.endy.training.virtualaccount.entity.VirtualAccount;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/delete-data-transaksi.sql", 
        "classpath:/sql/sample-data-transaksi.sql"
    }
)
public class VirtualAccountDaoTests {
    @Autowired private NasabahDao nasabahDao;
    @Autowired private RekeningDao rekeningDao;
    @Autowired private VirtualAccountDao virtualAccountDao;

    @Test
    public void testCariVirtualAccountDenganJenisTertentu(){
        List<VirtualAccount> hasil = virtualAccountDao.findByJenisVirtualAccount(JenisVirtualAccount.CLOSED);
        System.out.println("Jumlah hasil query : "+hasil.size());
        Assertions.assertEquals(Integer.valueOf(4), hasil.size());
    }

    @Test
    public void testCariVirtualAccountDenganNamaNasabah(){
        List<VirtualAccount> hasil = virtualAccountDao.findByRekeningNasabahNamaContainingIgnoreCase("nas");
        Assertions.assertEquals(Integer.valueOf(6), hasil.size());

        List<VirtualAccount> hasil2 = virtualAccountDao.findByRekeningNasabahNamaContainingIgnoreCase("001");
        Assertions.assertEquals(Integer.valueOf(4), hasil2.size());
        
        List<VirtualAccount> hasil3 = virtualAccountDao.findByRekeningNasabahNamaContainingIgnoreCase("99");
        Assertions.assertEquals(Integer.valueOf(0), hasil3.size());

        List<VirtualAccount> hasil4 = virtualAccountDao.findByRekeningNasabahNamaLike("99");
        Assertions.assertEquals(Integer.valueOf(0), hasil4.size());
    }

    @Test
    public void testCariVaDenganTanggalLahirNasabah(){
        List<VirtualAccount> hasil = virtualAccountDao
        .cariVaDenganTanggalLahirNasabahAntara(LocalDate.of(1991, 1, 1), LocalDate.of(1993, 12, 31));
        Assertions.assertEquals(Integer.valueOf(2), hasil.size());

        List<String> hasil2 = virtualAccountDao
        .cariNomorVaDenganTanggalLahirNasabahAntara(LocalDate.of(1991, 1, 1), LocalDate.of(1993, 12, 31));
        Assertions.assertEquals(Integer.valueOf(2), hasil.size());
        for(String nomor : hasil2) {
            System.out.println("Nomor VA : "+nomor);
        }
    }

    @Test
    public void testCariBerdasarkanSaldoRekening(){
        Page<VirtualAccount> hasil = virtualAccountDao
        .findByRekeningSaldoBetween(new BigDecimal(120000), new BigDecimal(200000), PageRequest.of(0, 10));
        Assertions.assertEquals(Long.valueOf(2), hasil.getTotalElements());
    }

    @Test
    public void testHitungJumlahVaOutstanding(){
        Long hasil = virtualAccountDao.hitungJumlahVaYangMasihAdaOutstanding();
        Assertions.assertEquals(6, hasil);
    }
}
