package com.muhardin.endy.training.virtualaccount.dao;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import com.muhardin.endy.training.virtualaccount.dto.UsernameRolenameDto;
import com.muhardin.endy.training.virtualaccount.dto.UsernameRolenameInterface;
import com.muhardin.endy.training.virtualaccount.entity.Role;
import com.muhardin.endy.training.virtualaccount.entity.User;
import com.muhardin.endy.training.virtualaccount.entity.UserPassword;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/delete-data-user-role-permission.sql", 
        "classpath:/sql/sample-data-user-role-permission.sql"
    }
)
public class UserDaoTests {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserPasswordDao userPasswordDao;

    @Test
    public void testDeleteUser(){
        User u = userDao.findById("u001").get();
        userDao.delete(u);
    }

    @Test
    public void testUnsetPassword(){
        User u = userDao.findById("u001").get();

        System.out.println("ID UserPassword lama : "+u.getUserPassword().getId());

        u.setUserPassword(null); // ini akan menghapus user_password karena tidak di-refer lagi oleh user
        userDao.save(u);
    }

    @Test
    public void testInsertUser(){
        User u = new User();
        u.setUsername("user001");
        u.setActive(true);
        
        UserPassword up = new UserPassword();
        up.setUser(u);
        up.setHashedPassword("123");

        u.setUserPassword(up);

        Role supervisor = roleDao.findById("r002").get();
        u.setRole(supervisor);
        
        //userPasswordDao.save(up); // tidak perlu kalau CascadeType.PERSIST
        userDao.save(u);
        
    }

    @Test
    public void testProjectionDenganJpqlDto(){
        List<UsernameRolenameDto> hasil 
            = userDao.cariUsernameDanRolenameJpql();
        Assertions.assertEquals(3, hasil.size());

        for(UsernameRolenameDto u : hasil){
            System.out.println(u.toString());
            System.out.println("Username : "+u.getUsername());
            System.out.println("Role : "+u.getRoleName());
        }
    }

    @Test
    public void testProjectionDenganSQLInterface(){
        List<UsernameRolenameInterface> hasil 
            = userDao.cariUsernameDanRolenameSql();
        Assertions.assertEquals(3, hasil.size());

        for(UsernameRolenameInterface u : hasil){
            System.out.println(u);
            System.out.println("Username : "+u.getUsername());
            System.out.println("Role : "+u.getRole_Name());
        }
    }
}
