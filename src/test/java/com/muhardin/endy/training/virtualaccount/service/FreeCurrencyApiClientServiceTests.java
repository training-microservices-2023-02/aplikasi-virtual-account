package com.muhardin.endy.training.virtualaccount.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FreeCurrencyApiClientServiceTests {
    @Autowired private FreeCurrencyApiClientService service;

    @Test
    public void testGetCurrency(){
        Map<String, BigDecimal> hasil = service.ambilDataCurrency();
        System.out.println(hasil.get("IDR"));
        
    }
}
