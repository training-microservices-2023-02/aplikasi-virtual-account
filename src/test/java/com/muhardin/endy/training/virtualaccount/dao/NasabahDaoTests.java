package com.muhardin.endy.training.virtualaccount.dao;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.jdbc.Sql;

import com.muhardin.endy.training.virtualaccount.entity.JenisKelamin;
import com.muhardin.endy.training.virtualaccount.entity.Nasabah;

@SpringBootTest
@Sql(
    scripts = {
        "classpath:/sql/delete-data-transaksi.sql", 
        "classpath:/sql/sample-data-transaksi.sql"
    }
)
public class NasabahDaoTests {
    @Autowired private NasabahDao nasabahDao;


    @Test
    public void testInsertNasabah(){
        Nasabah n = new Nasabah();
        n.setKode("9990001");
        n.setNama("Nasabah 991");
        n.setJenisKelamin(JenisKelamin.WANITA);
        n.setTanggalLahir(LocalDate.of(1990, 12, 31));
        nasabahDao.save(n);
    }

    @Test
    public void testUpdateNasabah(){
        Nasabah n = nasabahDao.findById("n002").get();
        n.setNama(n.getNama() + "-edit");
        nasabahDao.save(n);
    }

    @Test
    public void testQueryByExample(){
        Nasabah n = new Nasabah();
        n.setKode("123450001");
        Example<Nasabah> contohNasabah = Example.of(n);

        List<Nasabah> hasil = nasabahDao.findAll(contohNasabah);
        Assertions.assertEquals(1, hasil.size());
    }

    @Test
    public void testQueryByExampleMatcher(){
        Nasabah n = new Nasabah();
        n.setKode("0001");
        n.setNama("Nasabah");
        
        ExampleMatcher matcher = ExampleMatcher.matching()
        .withIgnoreCase()
        .withStringMatcher(StringMatcher.ENDING);

        List<Nasabah> hasil = nasabahDao.findAll(Example.of(n, matcher));
        Assertions.assertEquals(0, hasil.size());

        ExampleMatcher matcher2 = ExampleMatcher.matchingAny()
        .withIgnoreCase()
        .withMatcher("kode", m -> m.endsWith())
        .withMatcher("nama", m -> m.startsWith());

        Pageable page = PageRequest.of(0, 5);
        Page<Nasabah> hasil2 = nasabahDao.findAll(Example.of(n, matcher2), page);
        Assertions.assertEquals(4, hasil2.getNumberOfElements());
    }
}
