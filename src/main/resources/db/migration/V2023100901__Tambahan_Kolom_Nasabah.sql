create table provinsi (
    id varchar(36), 
    kode varchar(50) not null,
    nama varchar(100) not null,
    primary key (id)
);

alter table nasabah 
add column id_provinsi varchar(36);

alter table nasabah 
add column jenis_kelamin varchar(10);

alter table nasabah 
add column tanggal_lahir date;

alter table nasabah 
add column alamat text;

alter table nasabah 
add foreign key (id_provinsi) references provinsi(id);