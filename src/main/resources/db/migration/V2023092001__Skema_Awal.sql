create table nasabah (
    id varchar(36), 
    kode varchar(50) not null,
    nama varchar(100) not null,
    primary key (id)
);

create table virtual_account (
    id varchar(36),
    id_nasabah varchar(36) not null,
    nomor_account varchar(50) not null,
    nama_account varchar(100) not null,
    keterangan varchar(255) not null,
    nilai decimal(19,2) not null,
    berlaku_sampai date not null,
    primary key (id),
    foreign key (id_nasabah) references nasabah(id)
);