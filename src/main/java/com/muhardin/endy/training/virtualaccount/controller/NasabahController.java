package com.muhardin.endy.training.virtualaccount.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.muhardin.endy.training.virtualaccount.dao.NasabahDao;
import com.muhardin.endy.training.virtualaccount.dao.ProvinsiDao;
import com.muhardin.endy.training.virtualaccount.entity.Nasabah;
import com.muhardin.endy.training.virtualaccount.entity.Provinsi;

import jakarta.annotation.PostConstruct;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller 
public class NasabahController {

    @Value("${lokasi.upload}")
    private String lokasiUpload;

    @Autowired
    private NasabahDao nasabahDao;

    @Autowired 
    private ProvinsiDao provinsiDao;

    // inisialisasi logger secara manual
    //private static final Logger log = LoggerFactory.getLogger(NasabahController.class);

    // Pageable : menangkap parameter berikut
    // page : nomor halaman
    // size : jumlah baris dalam satu halaman
    // contoh : http://localhost:8080/nasabah/list?page=1&size=5
    // referensi : https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#core.web.basic.paging-and-sorting


    @PostConstruct
    public void inisialisasi(){
        File folderUpload = new File(lokasiUpload);
        log.debug("lokasiUpload : {}", folderUpload.getAbsolutePath());
        folderUpload.mkdirs();
    }

    @GetMapping("/nasabah/list")
    @PreAuthorize("hasAuthority('VIEW_NASABAH')")
    public ModelMap daftarNasabah(Pageable page){
        ModelMap mm = new ModelMap();

        Page<Nasabah> hasilQueryDb = nasabahDao.findAll(page);
        mm.addAttribute("daftarNasabah", hasilQueryDb);

        return mm;
    }

    // request param tipe datanya langsung entity
    // spring otomatis akan melakukan query findById sesuai dengan isi parameter
    @GetMapping("/nasabah/view")
    @PreAuthorize("hasAuthority('VIEW_NASABAH')")
    public void infoNasabah(@RequestParam(value = "id", required = true) Nasabah nasabah, ModelMap mm){
        // tidak perlu dicek, harusnya id tidak boleh null
        // dan nasabah dengan id tersebut tidak boleh null
        //if(nasabah != null) {
            mm.addAttribute(nasabah);
        //}

        // bila nasabah null, maka spring akan mengembalikan response code 400
        // 400 = invalid request, client/user harus memperbaiki sebelum request ulang
        // 500 = error server. Programmer aplikasi server harus memperbaiki
        // setelah diperbaiki, client/user bisa request ulang dengan request yang sama
    }

    @GetMapping("/nasabah/form")
    @PreAuthorize("hasAuthority('EDIT_NASABAH')")
    public ModelMap tampilkanFormInput(String id, Authentication currentUser){
        ModelMap mm = new ModelMap();
        mm.addAttribute("nasabah", new Nasabah());

        log.debug("Isi variabel id : {}", id);
        log.debug("User yang sedang login : {}", currentUser.getName());

        if(StringUtils.hasText(id)) {  // menggunakan library
        //if(id != null && !"".equals(id.trim()) ) { // cara manual
            Optional<Nasabah> optNasabah = nasabahDao.findById(id.trim());
            log.debug("Nasabah ada ? {}",optNasabah.isPresent());
            if(optNasabah.isPresent()) {
                mm.addAttribute("nasabah", optNasabah.get());
            }
        }

        return mm;
    }

    @PostMapping("/nasabah/form")
    @PreAuthorize("hasAuthority('EDIT_NASABAH')")
    public String prosesFormInput(@Valid Nasabah n, BindingResult validasi, 
                        @RequestParam("ktp") MultipartFile fileKtp) throws IllegalStateException, IOException{
        log.debug("Memproses form input nasabah");
        log.debug("Id nasabah : {}", n.getId());
        log.debug("Kode nasabah : {}", n.getKode());
        log.debug("Nama nasabah : {}", n.getNama());
        log.debug("Tanggal lahir : {}", n.getTanggalLahir());

        if(fileKtp != null) {
            log.debug("Nama file asli : {}", fileKtp.getOriginalFilename());
            log.debug("Ukuran file : {}", fileKtp.getSize());
            log.debug("Jenis file : {}", fileKtp.getContentType());
            
            String extensionFileKtp = ".bin";
            if(fileKtp.getOriginalFilename() != null) {
                Integer indexExtension = fileKtp.getOriginalFilename().lastIndexOf(".");
                if(indexExtension != -1) {
                    extensionFileKtp = fileKtp.getOriginalFilename().substring(indexExtension);
                }
            }
            log.debug("Extension file : {}", extensionFileKtp);

            String namafileHasilUpload = UUID.randomUUID().toString() + extensionFileKtp;
            n.setFileKtp(namafileHasilUpload);
            n.setTypeFileKtp(fileKtp.getContentType());
            fileKtp.transferTo(new File(lokasiUpload + File.separator + namafileHasilUpload));
        }
        
        if(validasi.hasErrors()){
            for(ObjectError err : validasi.getAllErrors()) {
                log.debug("Error di validasi {}, object {}, message : {}", err.getCode(), err.getObjectName(), err.getDefaultMessage());
            }
            return "nasabah/form";
        }

        nasabahDao.save(n);

        return "redirect:list";
    }

    @GetMapping("/nasabah/{id}/ktp")
    public ResponseEntity<byte[]> viewKtp(@PathVariable("id") Nasabah nasabah) throws IOException {
        String namafile = nasabah.getFileKtp();
        File fileKtp = new File(lokasiUpload + File.separator + namafile);

        return ResponseEntity.ok()
        .contentType(MediaType.valueOf(nasabah.getTypeFileKtp()))
        .body(Files.readAllBytes(Paths.get(fileKtp.getAbsolutePath())));
    }

    @GetMapping("/nasabah/excel")
    public ResponseEntity<byte[]> downloadExcel(){
        String namafileOutput = "nasabah-"+DateTimeFormatter.BASIC_ISO_DATE.format(LocalDate.now())+".xlsx";
        
        // data yang akan masuk ke excel
        Iterable<Nasabah> dataNasabah = nasabahDao.findAll();

        // buat file excel
        try (Workbook wb = new XSSFWorkbook()) {
            Sheet sheet1 = wb.createSheet(WorkbookUtil.createSafeSheetName("nasabah"));
            
            // isi header
            Row row = sheet1.createRow(0);
            row.createCell(0).setCellValue("Kode Nasabah");
            row.createCell(1).setCellValue("Nama Nasabah");
            row.createCell(2).setCellValue("Jenis Kelamin");
            row.createCell(3).setCellValue("Tanggal Lahir");
            row.createCell(4).setCellValue("Alamat");

            // isi row dengan data
            Integer baris = 1;
            for(Nasabah n : dataNasabah){
                Row r = sheet1.createRow(baris++);
                r.createCell(0).setCellValue(n.getKode());
                r.createCell(1).setCellValue(n.getNama());
                r.createCell(2).setCellValue(n.getJenisKelamin().toString());
                r.createCell(3).setCellValue(DateTimeFormatter.ISO_DATE.format(n.getTanggalLahir()));
                r.createCell(4).setCellValue(n.getAlamat());
            }

            // Konversi ke byte[] supaya bisa dikirim melalui ResponseEntity
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            wb.write(baos);
            wb.close();
            return ResponseEntity.ok()
            // Content-disposition : 
            // - inline : tampilkan di browser (kalau browsernya bisa)
            // - attachment : popup dialog download
            .header("Content-Disposition", "inline; filename="+namafileOutput)
            .contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            .body(baos.toByteArray());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    @ModelAttribute("daftarProvinsi")
    public Iterable<Provinsi> daftarProvinsi(){
        return provinsiDao.findAll();
    }
}
