package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.repository.CrudRepository;

import com.muhardin.endy.training.virtualaccount.entity.AuditLog;

public interface AuditLogDao extends CrudRepository<AuditLog, String> {
    
}
