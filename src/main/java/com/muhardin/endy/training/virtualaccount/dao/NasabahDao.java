package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.muhardin.endy.training.virtualaccount.entity.Nasabah;

public interface NasabahDao extends JpaRepository<Nasabah, String>{
    
}
