package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.training.virtualaccount.entity.PembayaranDebit;

public interface PembayaranDebitDao extends JpaRepository<PembayaranDebit, String> {
    
}
