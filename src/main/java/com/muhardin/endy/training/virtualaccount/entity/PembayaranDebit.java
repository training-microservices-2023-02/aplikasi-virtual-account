package com.muhardin.endy.training.virtualaccount.entity;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Entity @Data
public class PembayaranDebit extends Pembayaran {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String bankPenerbit;
    private String nomorKartu;
    private String referenceNumber;
}
