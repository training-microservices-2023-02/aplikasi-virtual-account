package com.muhardin.endy.training.virtualaccount.service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class RekapService {

    // jalan pada jam 20:24 setiap hari
    @Scheduled(cron = "0 45 20 * * * ")
    public void prosesEndOfDay(){
        log.debug("Menjalankan proses end of day");
    }

    @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.SECONDS)
    public void kirimNotifikasiPayment() throws Exception{
        // query tabel notifikasi
        // yang statusnya BELUM_TERKIRIM
        // kirim email ke nasabah
        // kemudian update status menjadi SUDAH_TERKIRIM
        log.debug("Kirim notifikasi mulai");
        Thread.sleep(20 * 1000);
        log.debug("Kirim notifikasi selesai");
    }

    @Scheduled(fixedRate = 5, timeUnit = TimeUnit.SECONDS)
    public void cekTransferMasuk() throws Exception{
        // akses external service untuk memeriksa transaksi
        // di core bank app
        String idProses = UUID.randomUUID().toString();
        log.debug("Cek transaksi "+ idProses +" mulai");
        Thread.sleep(20 * 1000);
        log.debug("Cek transaksi "+ idProses +" selesai");
    }
}
