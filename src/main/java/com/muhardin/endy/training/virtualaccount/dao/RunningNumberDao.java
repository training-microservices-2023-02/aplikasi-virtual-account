package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import com.muhardin.endy.training.virtualaccount.entity.RunningNumber;

import jakarta.persistence.LockModeType;

public interface RunningNumberDao extends CrudRepository<RunningNumber, String> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    RunningNumber findByPrefix(String prefix);
}
