package com.muhardin.endy.training.virtualaccount.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.muhardin.endy.training.virtualaccount.dao.PembayaranDao;
import com.muhardin.endy.training.virtualaccount.dao.VirtualAccountDao;
import com.muhardin.endy.training.virtualaccount.entity.Pembayaran;
import com.muhardin.endy.training.virtualaccount.entity.VirtualAccountStatus;
import com.muhardin.endy.training.virtualaccount.exception.PembayaranBerlebihException;
import com.muhardin.endy.training.virtualaccount.exception.SudahLunasException;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class PembayaranService {

    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private AuditLogService auditLogService;

    // walaupun SudahLunasException adalah checked exception, kita rollback
    // defaultnya checked exception tidak menyebabkan rollback
    // cara manualnya seperti ini : https://sourceforge.net/p/playbilling/code/HEAD/tree/trunk/web/WEB-INF/conf/ctx-billing.xml
    @Transactional(rollbackFor = SudahLunasException.class)
    public void bayar(Pembayaran p) throws SudahLunasException, PembayaranBerlebihException {
        auditLogService.log("[PAYMENT] - [START] : VA = "+p.getVirtualAccount().getNomor()+", AMT = "+p.getNilai());
        // normalnya ini paling bawah setelah validasi
        // dipindah ke atas untuk mendemokan rollback
        pembayaranDao.save(p);
        
        // validasi status VAnya apakah sudah lunas
        if(VirtualAccountStatus.PAID_FULLY
            .equals(p.getVirtualAccount().getVirtualAccountStatus())) {
            log.info("VA {} sudah lunas", p.getVirtualAccount().getNomor());
            auditLogService.log("[PAYMENT] - [ERROR] : VA = "+p.getVirtualAccount().getNomor()+", ERR = ALREADY PAID");
            throw new SudahLunasException("VA "+p.getVirtualAccount().getNomor()+ " sudah lunas");
        }

        // cek dulu apakah dibayar penuh atau sebagian
        BigDecimal sisaPembayaran = p.getVirtualAccount().getNilaiSisa();
        BigDecimal nilaiBayar = p.getNilai();
        if(sisaPembayaran.compareTo(nilaiBayar) > 0) {
            p.getVirtualAccount().setVirtualAccountStatus(VirtualAccountStatus.PAID_PARTIALLY);
        } else if(sisaPembayaran.compareTo(nilaiBayar) == 0) {
            p.getVirtualAccount().setVirtualAccountStatus(VirtualAccountStatus.PAID_FULLY);
        } else {
            auditLogService.log("[PAYMENT] - [ERROR] : VA = "+p.getVirtualAccount().getNomor()+", ERR = AMOUNT EXCEEDED");

            throw new PembayaranBerlebihException("VA "+p.getVirtualAccount().getNomor()
                + " nilai sisa : "+p.getVirtualAccount().getNilaiSisa() 
                + ", pembayaran : "+p.getNilai());
        }
        BigDecimal akumulasiPembayaran = p.getVirtualAccount().getAkumulasiPembayaran().add(p.getNilai());
        p.getVirtualAccount().setAkumulasiPembayaran(akumulasiPembayaran);

        BigDecimal sisa = p.getVirtualAccount().getNilaiTotal().subtract(akumulasiPembayaran);
        p.getVirtualAccount().setNilaiSisa(sisa);

        virtualAccountDao.save(p.getVirtualAccount());
        
        auditLogService.log("[PAYMENT] - [SUCCESS] : VA = "+p.getVirtualAccount().getNomor());
    }
}
