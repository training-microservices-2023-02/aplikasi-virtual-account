package com.muhardin.endy.training.virtualaccount.entity;

import java.math.BigDecimal;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.data.annotation.Version;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity 
//@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED) 
@Audited
@Data
public class Rekening {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_nasabah")
    private Nasabah nasabah;

    private String nomor;

    @NotNull @Min(0)
    private BigDecimal saldo;

    @Version Long version;
}
