package com.muhardin.endy.training.virtualaccount.exception;

// unchecked exception
// tidak wajib dihandle 
public class PembayaranBerlebihException extends RuntimeException {
    public PembayaranBerlebihException(String message){
        super(message);
    }
}
