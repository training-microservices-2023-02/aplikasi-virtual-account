package com.muhardin.endy.training.virtualaccount.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.header.writers.XXssProtectionHeaderWriter;

@Configuration 
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {
    private static final String SQL_LOGIN = """
        select u.username, up.hashed_password, u.active as enabled 
        from user u inner join user_password up on u.id = up.id 
        where u.username = ?
            """;
    
    private static final String SQL_PERMISSION = """
        select u.username, p.permission_value as authority 
        from user u inner join role r on u.id_role = r.id 
        inner join role_permission rp on r.id = rp.id_role 
        inner join permission p on rp.id_permission = p.id 
        where u.username = ?;
            """;

    @Bean
	public UserDetailsService userDetailsService(DataSource dataSource) {
		JdbcUserDetailsManager userDatabase = new JdbcUserDetailsManager(dataSource);
        userDatabase.setUsersByUsernameQuery(SQL_LOGIN);
        userDatabase.setAuthoritiesByUsernameQuery(SQL_PERMISSION);
        return userDatabase;
	}

    @Bean
    SecurityFilterChain web(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((authorize) -> 
            authorize
            .requestMatchers("/public/**").permitAll()
            .anyRequest().authenticated()
        //).formLogin(Customizer.withDefaults()) // pakai form login bawaan
        ).formLogin(login -> 
            login.loginPage("/login")
            .defaultSuccessUrl("/nasabah/list", true)
            .permitAll()
        ).logout(Customizer.withDefaults())  
        .sessionManagement(session -> session
            .maximumSessions(1) // tidak boleh login di 2 tempat
            .maxSessionsPreventsLogin(true) // login pertama menang, login kedua ditolak
        // XSS Protection
        // https://docs.spring.io/spring-security/reference/servlet/exploits/headers.html#servlet-headers-xss-protection
        // https://www.baeldung.com/spring-prevent-xss
        ).headers(h -> h 
            .xssProtection(x -> x
                .headerValue(XXssProtectionHeaderWriter.HeaderValue.ENABLED_MODE_BLOCK))
            .contentSecurityPolicy(csp -> csp
                .policyDirectives("script-src 'self'")
            )
        );

        return http.build();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
}
