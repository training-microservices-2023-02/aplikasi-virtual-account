package com.muhardin.endy.training.virtualaccount.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muhardin.endy.training.virtualaccount.entity.JenisVirtualAccount;
import com.muhardin.endy.training.virtualaccount.entity.VirtualAccount;

public interface VirtualAccountDao extends JpaRepository<VirtualAccount, String>{
    List<VirtualAccount> findByJenisVirtualAccount(JenisVirtualAccount j);
    List<VirtualAccount> findByRekeningNasabahNamaContainingIgnoreCase(String nama);
    List<VirtualAccount> findByRekeningNasabahNamaLike(String nama);

    Page<VirtualAccount> findByRekeningSaldoBetween(BigDecimal mulai, BigDecimal sampai, Pageable page);

    // JPQL : Java Persistence Query Language
    // bukan SQL
    @Query(value = "select va from VirtualAccount va where va.rekening.nasabah.tanggalLahir between :mulai and :sampai", nativeQuery = false)
    List<VirtualAccount> cariVaDenganTanggalLahirNasabahAntara(@Param("mulai") LocalDate mulai, @Param("sampai") LocalDate sampai);

    @Query("select va.nomor from VirtualAccount va where va.rekening.nasabah.tanggalLahir between :mulai and :sampai")
    List<String> cariNomorVaDenganTanggalLahirNasabahAntara(@Param("mulai") LocalDate mulai, @Param("sampai") LocalDate sampai);

    // SQL, menggunakan nativeQuery = true
    @Query(value = "select count(*) from virtual_account where nilai_sisa > 0", nativeQuery = true)
    Long hitungJumlahVaYangMasihAdaOutstanding();
    Optional<VirtualAccount> findByNomor(String virtualAccountNumber);

}
