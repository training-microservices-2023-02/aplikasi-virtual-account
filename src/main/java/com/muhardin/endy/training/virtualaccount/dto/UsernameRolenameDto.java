package com.muhardin.endy.training.virtualaccount.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString @Getter @AllArgsConstructor
public class UsernameRolenameDto {
    private String username;
    private String roleName;    

/* //pakai lombok saja
    public UsernameRolenameDto(String u, String r) {
        this.username = u;
        this.roleName = r;
    }
*/
}
