package com.muhardin.endy.training.virtualaccount.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import jakarta.annotation.PostConstruct;

@Service 
public class FreeCurrencyApiClientService {
    private static final String SERVER_CURRENCY = "https://api.freecurrencyapi.com";
    private static final String URL_CURRENCY = "/v1/latest";

    @Value("${freecurrency.api.key}")
    private String apiKey;

    private WebClient webClient;

    @PostConstruct
    public void inisialisasiWebclient(){
        webClient = WebClient.builder()
        .baseUrl(SERVER_CURRENCY)
        .defaultHeader("apikey", apiKey)
        .build();
    }

    public Map<String, BigDecimal> ambilDataCurrency(){
        return webClient.get()
        .uri(URL_CURRENCY)
        .retrieve()
        .bodyToMono(new ParameterizedTypeReference<Map<String, Map<String, BigDecimal>>>(){}).block()
        .get("data");
    }
}