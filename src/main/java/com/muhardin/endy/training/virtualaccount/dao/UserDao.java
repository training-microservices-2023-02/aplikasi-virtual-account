package com.muhardin.endy.training.virtualaccount.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.muhardin.endy.training.virtualaccount.dto.UsernameRolenameDto;
import com.muhardin.endy.training.virtualaccount.dto.UsernameRolenameInterface;
import com.muhardin.endy.training.virtualaccount.entity.User;

public interface UserDao extends JpaRepository<User, String> {
    static final String SQL_SELECT_USERNAME_ROLENAME = """
        select u.username, r.name as role_name 
        from user u inner join role r on u.id_role = r.id
            """;
    
    static final String JPQL_SELECT_USERNAME_ROLENAME 
    = "select new com.muhardin.endy.training.virtualaccount.dto.UsernameRolenameDto(u.username, u.role.name) "
    + "from User u";

    @Query(JPQL_SELECT_USERNAME_ROLENAME)
    List<UsernameRolenameDto> cariUsernameDanRolenameJpql();

    @Query(value = SQL_SELECT_USERNAME_ROLENAME, nativeQuery = true)
    List<UsernameRolenameInterface> cariUsernameDanRolenameSql();
}
