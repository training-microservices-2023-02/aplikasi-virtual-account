package com.muhardin.endy.training.virtualaccount.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.muhardin.endy.training.virtualaccount.dao.NasabahDao;
import com.muhardin.endy.training.virtualaccount.entity.Nasabah;

@RestController
public class NasabahApiController {

    @Autowired
    private NasabahDao nasabahDao;

    @GetMapping("/api/nasabah/")
    public Iterable<Nasabah> dataNasabah(){
        Iterable<Nasabah> hasilQueryDb = nasabahDao.findAll();
        return hasilQueryDb;
    }

    @GetMapping("/api/nasabah/{id}")
    public Optional<Nasabah> cariNasabahById(@PathVariable String id){
        return nasabahDao.findById(id);
    }

    @PostMapping("/api/nasabah/")
    public void insertNasabah(@RequestBody Nasabah nasabah) {
        nasabahDao.save(nasabah);
    }
}
