package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.repository.CrudRepository;

import com.muhardin.endy.training.virtualaccount.entity.Provinsi;

public interface ProvinsiDao extends CrudRepository<Provinsi, String> {
    
}
