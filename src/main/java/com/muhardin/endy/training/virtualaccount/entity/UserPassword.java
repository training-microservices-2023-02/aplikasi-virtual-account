package com.muhardin.endy.training.virtualaccount.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Entity @Data
public class UserPassword {
    @Id
    private String id;

    @OneToOne(optional = false)
    @JoinColumn(name = "id")
    @MapsId
    private User user;

    private String hashedPassword;
}
