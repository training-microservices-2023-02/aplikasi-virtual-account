package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.muhardin.endy.training.virtualaccount.entity.Rekening;

public interface RekeningDao extends JpaRepository<Rekening, String>, RevisionRepository<Rekening, String, Long> {
    
}
