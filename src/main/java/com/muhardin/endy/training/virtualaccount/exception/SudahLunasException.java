package com.muhardin.endy.training.virtualaccount.exception;


// checked exception
// wajib dihandle
// cara handle ada 2 alternatif
// 1. catch : dihandle di tempat
// 2. throws : lempar ke method yang memanggil
public class SudahLunasException extends Exception {
    public SudahLunasException(String message) {
        super(message);
    }
}
