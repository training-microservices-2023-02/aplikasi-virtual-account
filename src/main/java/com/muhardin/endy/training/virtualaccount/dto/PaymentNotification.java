package com.muhardin.endy.training.virtualaccount.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PaymentNotification {

    //@JsonProperty("virtual_account_number") // tidak perlu karena sudah digantikan dengan @JsonNaming
    private String virtualAccountNumber;
    private String bank;
    private String cardNumber;
    private String referenceNumber;
    private BigDecimal paymentAmount;

    //@JsonProperty("transaction_time")
    @JsonFormat(pattern = "yyyyMMddHHmmss")
    private LocalDateTime transactionTime;
}
