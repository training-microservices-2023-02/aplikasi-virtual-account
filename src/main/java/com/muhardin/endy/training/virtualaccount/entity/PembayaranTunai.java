package com.muhardin.endy.training.virtualaccount.entity;

import java.math.BigDecimal;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Entity @Data
public class PembayaranTunai extends Pembayaran {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private BigDecimal dibayar;

    public BigDecimal hitungKembalian(){
        return dibayar.subtract(getNilai());
    }
}
