package com.muhardin.endy.training.virtualaccount.entity;

import java.math.BigDecimal;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Entity @Data
public class VirtualAccount {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_rekening")
    private Rekening rekening;
    private String nomor;

    @Enumerated(EnumType.STRING)
    private JenisVirtualAccount jenisVirtualAccount;
    private BigDecimal nilaiTotal;
    private BigDecimal nilaiSisa;
    private BigDecimal akumulasiPembayaran = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private VirtualAccountStatus virtualAccountStatus;
}
