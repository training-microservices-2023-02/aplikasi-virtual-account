package com.muhardin.endy.training.virtualaccount.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.muhardin.endy.training.virtualaccount.dao.AuditLogDao;
import com.muhardin.endy.training.virtualaccount.entity.AuditLog;

@Service 
public class AuditLogService {
    @Autowired private AuditLogDao auditLogDao;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void log(String message) {
        AuditLog lunas = new AuditLog();
        lunas.setMessage(message);
        auditLogDao.save(lunas);
    }
}
