package com.muhardin.endy.training.virtualaccount.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.muhardin.endy.training.virtualaccount.dao.RunningNumberDao;
import com.muhardin.endy.training.virtualaccount.entity.RunningNumber;

@Service 
public class RunningNumberService {
    private static final Integer length = 4;

    @Autowired private RunningNumberDao runningNumberDao;

    @Transactional
    public String generate(String prefix){
        RunningNumber rn = runningNumberDao.findByPrefix(prefix);
        if(rn == null) {
            rn = new RunningNumber();
            rn.setPrefix(prefix);
            rn.setLastNumber(0L);
        }
        rn.setLastNumber(rn.getLastNumber() + 1);
        runningNumberDao.save(rn);
        return prefix + String.format("%1$" + length + "s", rn.getLastNumber()).replace(" ", "0");
    }
}
