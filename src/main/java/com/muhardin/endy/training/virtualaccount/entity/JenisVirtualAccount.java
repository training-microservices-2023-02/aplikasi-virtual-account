package com.muhardin.endy.training.virtualaccount.entity;

public enum JenisVirtualAccount {
    OPEN, CLOSED, INSTALLMENT
}
