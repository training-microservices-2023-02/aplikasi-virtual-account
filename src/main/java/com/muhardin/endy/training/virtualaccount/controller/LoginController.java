package com.muhardin.endy.training.virtualaccount.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
    @GetMapping("/login")
    public void displayLoginPage(){
        
    }

    @GetMapping("/logout")
    public void displayLogoutPage(){
        
    }
}
