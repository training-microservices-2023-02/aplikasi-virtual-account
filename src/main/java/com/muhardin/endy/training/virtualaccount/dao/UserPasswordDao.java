package com.muhardin.endy.training.virtualaccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.training.virtualaccount.entity.UserPassword;

public interface UserPasswordDao extends JpaRepository<UserPassword, String>{
    
}
