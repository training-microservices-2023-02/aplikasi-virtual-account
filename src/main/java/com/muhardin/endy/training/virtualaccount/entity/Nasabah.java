package com.muhardin.endy.training.virtualaccount.entity;

import java.time.Instant;
import java.time.LocalDate;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Basic;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Entity @Data 
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED) // relasi provinsi tidak perlu diaudit
@EntityListeners(AuditingEntityListener.class)
public class Nasabah {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(min = 3, max = 10)
    private String kode;

    @NotNull @NotEmpty @Size(min = 3, max = 50)
    private String nama;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    // @Basic
    private String fileKtp;
    private String typeFileKtp;
    
    private LocalDate tanggalLahir;

    private String alamat;

    @ManyToOne
    @JoinColumn(name = "id_provinsi")
    private Provinsi provinsi;

    @CreatedDate
    private Instant waktuDibuat;

    @LastModifiedDate
    private Instant terakhirUpdate;
}
