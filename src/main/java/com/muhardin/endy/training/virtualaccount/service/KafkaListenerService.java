package com.muhardin.endy.training.virtualaccount.service;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.virtualaccount.dao.PembayaranDebitDao;
import com.muhardin.endy.training.virtualaccount.dao.VirtualAccountDao;
import com.muhardin.endy.training.virtualaccount.dto.PaymentNotification;
import com.muhardin.endy.training.virtualaccount.entity.PembayaranDebit;
import com.muhardin.endy.training.virtualaccount.entity.VirtualAccount;
import com.muhardin.endy.training.virtualaccount.entity.VirtualAccountStatus;

import lombok.extern.slf4j.Slf4j;

@Service @Transactional @Slf4j
public class KafkaListenerService {

    @Autowired private ObjectMapper objectMapper;
    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PembayaranDebitDao pembayaranDebitDao;

    @KafkaListener(topics = "payment-notification")
    public void terimaPaymentNotification(String paymentMessage) {
        log.debug("Terima message : {}", paymentMessage);
        try {
            PaymentNotification payment = objectMapper.readValue(paymentMessage, PaymentNotification.class);
            log.debug("Payment object : {}", payment);

            Optional<VirtualAccount> optVa = virtualAccountDao.findByNomor(payment.getVirtualAccountNumber());
            if(!optVa.isPresent()) {
                log.warn("Virtual Account Number [{}] not found in database", payment.getVirtualAccountNumber());
                return;
            }

            // TODO : handle payment untuk va yang sudah lunas 
            VirtualAccount va = optVa.get();
            va.setAkumulasiPembayaran(va.getAkumulasiPembayaran().add(payment.getPaymentAmount()));
            va.setNilaiSisa(va.getNilaiSisa().subtract(payment.getPaymentAmount()));

            if(BigDecimal.ZERO.compareTo(va.getNilaiSisa()) == 0) {
                va.setVirtualAccountStatus(VirtualAccountStatus.PAID_FULLY);
            } else {
                // TODO : pembayaran kelebihan belum dihandle
                va.setVirtualAccountStatus(VirtualAccountStatus.PAID_PARTIALLY);
            }

            PembayaranDebit pd = new PembayaranDebit();
            pd.setBankPenerbit(payment.getBank());
            pd.setNilai(payment.getPaymentAmount());
            pd.setNomorKartu(payment.getCardNumber());
            pd.setVirtualAccount(va);
            pd.setWaktuTransaksi(payment.getTransactionTime());
            pd.setReferenceNumber(payment.getReferenceNumber());

            pembayaranDebitDao.save(pd);
            virtualAccountDao.save(va);

        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }
}
