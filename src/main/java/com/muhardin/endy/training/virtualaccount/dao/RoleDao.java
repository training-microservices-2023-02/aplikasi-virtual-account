package com.muhardin.endy.training.virtualaccount.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.training.virtualaccount.entity.Role;

public interface RoleDao extends JpaRepository <Role, String> {
    List<Role> findByDaftarPermissionIsEmpty();
}
