package com.muhardin.endy.training.virtualaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
public class AplikasiVirtualAccountApplication {

	@Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }

	public static void main(String[] args) {
		SpringApplication.run(AplikasiVirtualAccountApplication.class, args);
	}

}
