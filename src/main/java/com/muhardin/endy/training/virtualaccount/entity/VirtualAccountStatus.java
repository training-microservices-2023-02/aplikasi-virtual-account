package com.muhardin.endy.training.virtualaccount.entity;

public enum VirtualAccountStatus {
    UNPAID, PAID_PARTIALLY, PAID_FULLY
}
