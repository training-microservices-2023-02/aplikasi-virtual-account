package com.muhardin.endy.training.virtualaccount.dto;

public interface UsernameRolenameInterface {
    String getUsername();
    String getRole_Name();
}
