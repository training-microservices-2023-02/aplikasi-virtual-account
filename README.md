# Aplikasi Virtual Account

Pilihan modul untuk generate project

[![Spring Initializer](img/spring-generator.png )](img/spring-generator.png)

## Menjalankan Database Development

```
docker compose up
```

## Menjalankan Aplikasi

```
mvn clean spring-boot:run
```

## Skema Database Aplikasi VA ##

[![Skema Database](img/skema-aplikasi-va.png)](img/skema-aplikasi-va.png)

## Spring Boot Tech Stack ##

[![Spring Boot Tech Stack](img/spring-boot-tech-stack.png)](img/spring-boot-tech-stack.png)

[![Spring Data JPA](img/spring-boot-data-jpa.png)](img/spring-boot-data-jpa.png)

## Evolusi Metode Akses Database di Java ##

[![Akses Database Java](img/akses-database-java.png)](img/akses-database-java.png)

## Build dan Deploy Aplikasi 

1. Perintah untuk build

    ```
    mvn clean package -DskipTests
    ```

2. Sediakan konfigurasi sesuai environment, berupa file `application.properties` yang diletakkan di sebelah file `jar` hasil build. Misalnya kita override konfigurasi koneksi database, log level, dan limit upload. Sisanya tetap sesuai konfigurasi `application.properties` di dalam jar.

    ```
    spring.datasource.url=jdbc:mysql://localhost:33088/virtualaccountdb_uat
    spring.datasource.username=va-uat
    spring.datasource.password=va-uat-20231026

    logging.level.com.muhardin.endy.training.virtualaccount=INFO

    spring.servlet.multipart.max-file-size=100MB
    ```

3. Beberapa pilihan target deployment :

    * Physical / Virtual Server
    
        * Linux
        * Windows
    
    * Docker Compose
    * Kubernetes

4. Setup system service, supaya aplikasi jalan pada waktu server reboot/restart. 

    * Untuk Linux, caranya sebagai berikut:

        1. [Setup Nginx + SSL](https://software.endy.muhardin.com/devops/deployment-microservice-kere-hore-1/)
        2. [Setup Aplikasi Spring Boot sebagai system service](https://software.endy.muhardin.com/devops/deployment-microservice-kere-hore-2/)
    
    * Untuk Windows, bisa menggunakan Windows Service Wrapper atau Java Service Wrapper. Tutorialnya sebagai berikut:

        * [Windows Service Wrapper](https://www.baeldung.com/spring-boot-app-as-a-service)
        * [Java Service Wrapper](https://medium.com/@lk.snatch/jar-file-as-windows-service-bonus-jar-to-exe-1b7b179053e4)

5. Setup Windows Service

    * Instalasi Service

        ```
        aplikasi-virtual-account.exe install
        ```
    
    * Menjalankan Service

        ```
        aplikasi-virtual-account.exe start
        ```

    * [Referensi resmi](https://github.com/winsw/winsw)

## Background Process dan Scheduler ##

1. Untuk task yang butuh waktu lama, gunakan asynchronous method. 

    ```java
    @Async
    public void checkPaymentStatus(String paymentId) {
        // http call ke server mitra, makan waktu > 30 detik
    }
    ```

    Async method bisa pakai argumen, tapi tidak ada return value (`void`). Proses bisnis harus disesuaikan supaya ada status perantara (`IN_PROGRESS`). Mungkin juga perlu ada tabel database untuk melihat status background process.

2. Untuk task yang berulang setiap interval waktu tertentu atau jam tertentu, gunakan anotasi `@Scheduled`. Contoh :

    ```java
    // jalan pada jam 20:24 setiap hari
    @Scheduled(cron = "0 45 20 * * * ")
    public void prosesEndOfDay(){
        
    }

    @Scheduled(fixedDelay = 10, timeUnit = TimeUnit.SECONDS)
    public void kirimNotifikasiPayment() throws Exception{
        
    }

    @Scheduled(fixedRate = 5, timeUnit = TimeUnit.SECONDS)
    public void cekTransferMasuk() throws Exception{
        
    }
    ```

3. Kode program `@Scheduled` di atas hanya bisa digunakan di single instance. Tidak bisa direplikasi. Untuk aplikasi yang dijalankan dengan replication, gunakan beberapa alternatif berikut:

    * [Shedlock](https://github.com/lukas-krecan/ShedLock)
    * [db-scheduler](https://github.com/kagkarlsson/db-scheduler)
    * [JobRunr](https://www.jobrunr.io/en/documentation/configuration/spring/)

## Aspect Oriented Programming

[![AOP - Why](img/aop-why.png)](img/aop-why.png)

[![AOP - How](img/aop-how.png)](img/aop-how.png)

## Messaging dengan Kafka ##

[![Studi Kasus Kafka](img/studi-kasus-kafka.png)](img/studi-kasus-kafka.png)

Referensi : 
* [Contoh konfigurasi AOP Spring jadul](https://sourceforge.net/p/playbilling/code/HEAD/tree/trunk/web/WEB-INF/conf/ctx-billing.xml)
* [Penjelasan AOP. Episode 13 - 15](https://www.youtube.com/playlist?list=PL9oC_cq7OYbyhdZmCECQqp7OcS8J5QpAo)

# Summary #

Langkah-langkah pembuatan aplikasi:

1. Generate project Spring Boot [menggunakan initializer](https://start.spring.io)

    * Maven Project
    * Dependensi : database, flyway (optional), lombok, spring data jpa, spring web, thymeleaf

2. Siapkan koneksi database dan skema tabel (bila perlu)
3. Setup konfigurasi koneksi database di `application.properties`
4. Siapkan migration script (untuk db baru, db existing tidak perlu)
5. Buat Entity sesuai tabel database
6. Buat Dao
7. Buat Controller
8. Test akses URL sesuai mapping di controller